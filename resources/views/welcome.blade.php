@extends("layouts.master")
@section("content")


    <!-- Main content Start -->
    <div class="main-content">

        <!--Full width header Start-->

        <!--Full width header End-->

        <!-- Banner Section Start -->
        <div class="rs-banner main-home">
            <div class="container custom">
                <div class="content-wrap">
                    <div class="border-line"></div>
                    <h1 class="title">We maximize</h1>
                    <h3 class="sub-title">your business growth through technology</h3>
                    <div class="btn-part">
                        <a class="readon consultant" href="contact.html">Get in touch</a>
                    </div>
                </div>
            </div>
            <div class="animate-part">
                <div class="spiner dot">
                    <img class="rotate_Y" src="{{asset('public/images/banner/style1/dot-rect.png')}}" alt="images">
                </div>
                <div class="spiner tri-circle">
                    <img class="up-down" src="{{asset('public/images/banner/style1/tri-circle1.png')}}" alt="images">
                </div>
                <div class="spiner circle">
                    <img class="up-down" src="{{asset('public/images/banner/style1/circle1.png')}}" alt="images">
                </div>
                <div class="spiner line">
                    <img class="up-down" src="{{asset('public/images/banner/style1/line1.png')}}" alt="images">
                </div>
            </div>
        </div>
        <!-- Banner Section End -->

        <!-- About Section Start -->
{{--        <div class="rs-about main-home bg1 pt-110 pb-110 md-pt-70 md-pb-65">--}}
{{--            <div class="container">--}}
{{--                <div class="row y-middle">--}}
{{--                    <div class="col-lg-6 md-mb-50">--}}
{{--                        <div class="images-part">--}}
{{--                            <img src="{{asset('public/images/about/about.png')}}" alt="Images">--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="col-lg-6 pl-60 md-pl-15">--}}
{{--                        <div class="sec-title">--}}
{{--                            <h2 class="title pb-30">--}}
{{--                                We take your business to next level--}}
{{--                            </h2>--}}
{{--                            <span class="sub-text">5+ years experience in software development and IT consulting, and working with clients over the world. </span>--}}

{{--                            <p class="margin-0">Nor is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but occasionally circumstances occur in which toil and pain can procure him some great pleasure. Creation timelines for the digital consulting business theme 2021 bstandard lorem ipsum passage vary, with some citing the 15th century and others the 20th.</p>--}}
{{--                            <div class="btn-part mt-45">--}}
{{--                                <a class="readon consultant discover" href="about.html">Discover More</a>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
        <!-- About Section End -->

        <!-- About Section Start -->
        <div class="rs-about style2 pt-100 pb-100 md-pt-70 md-pb-70">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 pr-33 md-pr-15 md-mb-50">
                        <div class="images-part">
                            <img src="{{asset('public/images/about/about-3.png')}}" alt="Images">
                        </div>
                    </div>
                    <div class="col-lg-6 ">
                        <div class="sec-title">
                            <h2 class="title pb-22">
                                We help to implement your ideas into automation
                            </h2>
                            <span class="sub-text big">We donec pulvinar magna id leoersi pellentesque impered dignissim rhoncus euismod euismod eros vitae best consulting & financial services theme .</span>

                            <p class="margin-0 pt-15">Business ipsum dolor sit amet nsectetur cing elit. Suspe ndisse suscipit sagittis leo sit met entum is not estibulum dignity sim posuere cubilia durae. Leo sit met entum cubilia crae. At vero eos accusamus et iusto odio dignissimos provident.</p>
                            <div class="btn-part mt-45 md-mt-30">
                                <a class="readon consultant discover" href="about.html">Contact Us</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="rs-animation">
                <div class="animate-style">
                    <img class="scale" src="{{asset('public/images/about/tri-circle-1.png')}}" alt="About">
                </div>
            </div>
        </div>
        <!-- About Section End -->



        <!-- Premium Services Section Start -->
        <div id="rs-services" class="rs-services style2 gray-bg pt-100 pb-100 md-pt-70 md-pb-70">
            <div class="container">
                <div class="sec-title2 d-flex align-items-center mb-60 md-mb-40">
                    <div class="first-half">
                        <div class="sub-text">What We Offer</div>
                        <h2 class="title mb-0 md-pb-20">The best solutions for your business - <span>what we do.</span></h2>
                    </div>
                    <div class="last-half">
                        <p class="desc mb-0 pl-20 md-pl-15">5+ years experience in Software development and IT consulting, and working with clients over the world. Creation timelines for the digital consulting business theme 2021.</p>
                    </div>
                </div>
                <div class="rs-carousel owl-carousel"
                     data-loop="false"
                     data-items="3"
                     data-margin="30"
                     data-autoplay="true"
                     data-hoverpause="true"
                     data-autoplay-timeout="5000"
                     data-smart-speed="800"
                     data-dots="true"
                     data-nav="false"
                     data-nav-speed="false"

                     data-md-device="3"
                     data-md-device-nav="false"
                     data-md-device-dots="true"
                     data-center-mode="false"

                     data-ipad-device2="2"
                     data-ipad-device-nav2="false"
                     data-ipad-device-dots2="true"

                     data-ipad-device="2"
                     data-ipad-device-nav="false"
                     data-ipad-device-dots="true"

                     data-mobile-device="1"
                     data-mobile-device-nav="false"
                     data-mobile-device-dots="true">

                    <div class="service-wrap">
                        <div class="image-part">
                            <img src="{{asset('public/images/services/style2/2.jpg')}}" alt="">
                        </div>
                        <div class="content-part">
                            <h3 class="title"><a href="tax-strategy.html">Website Development</a></h3>
                            <div class="desc">We denounce with indignation on and dislike men who are so begui led and demoralized data.</div>
                        </div>
                    </div>
                    <div class="service-wrap">
                        <div class="image-part">
                            <img src="{{asset('public/images/services/style2/1.jpg')}}" alt="">
                        </div>
                        <div class="content-part">
                            <h3 class="title"><a href="business-planning.html">Custom Software Development</a></h3>
                            <div class="desc">We denounce with indignation on and dislike men who are so begui led and demoralized data.</div>
                        </div>
                    </div>
                    <div class="service-wrap">
                        <div class="image-part">
                            <img src="{{asset('public/images/services/style2/3.jpg')}}" alt="">
                        </div>
                        <div class="content-part">
                            <h3 class="title"><a href="financial-advices.html">Mobile Apps Development</a></h3>
                            <div class="desc">We denounce with indignation on and dislike men who are so begui led and demoralized data.</div>
                        </div>
                    </div>

                </div>
                <div class="rs-patter-section style1 pt-75">
                    <div class="container custom">
                        <div class="rs-carousel owl-carousel"
                             data-loop="true"
                             data-items="5"
                             data-margin="30"
                             data-autoplay="true"
                             data-hoverpause="true"
                             data-autoplay-timeout="5000"
                             data-smart-speed="800"
                             data-dots="false"
                             data-nav="false"
                             data-nav-speed="false"

                             data-md-device="5"
                             data-md-device-nav="false"
                             data-md-device-dots="false"
                             data-center-mode="false"

                             data-ipad-device2="4"
                             data-ipad-device-nav2="false"
                             data-ipad-device-dots2="true"

                             data-ipad-device="3"
                             data-ipad-device-nav="false"
                             data-ipad-device-dots="false"

                             data-mobile-device="2"
                             data-mobile-device-nav="false"
                             data-mobile-device-dots="false">
                            <div class="logo-img">
                                <a href="https://rstheme.com/">
                                    <img class="hovers-logos rs-grid-img" src="{{asset('public/images/partner/main-home/1.png')}}" title="" alt="">
                                    <img class="mains-logos rs-grid-img " src="{{asset('public/images/partner/main-home/1.png')}}" title="" alt="">
                                </a>
                            </div>
                            <div class="logo-img">
                                <a href="https://rstheme.com/">
                                    <img class="hovers-logos rs-grid-img" src="{{asset('public/images/partner/main-home/2.png')}}" title="" alt="">
                                    <img class="mains-logos rs-grid-img " src="{{asset('public/images/partner/main-home/2.png')}}" title="" alt="">
                                </a>
                            </div>
                            <div class="logo-img">
                                <a href="https://rstheme.com/">
                                    <img class="hovers-logos rs-grid-img" src="{{asset('public/images/partner/main-home/3.png')}}" title="" alt="">
                                    <img class="mains-logos rs-grid-img " src="{{asset('public/images/partner/main-home/3.png')}}" title="" alt="">
                                </a>
                            </div>
                            <div class="logo-img">
                                <a href="https://rstheme.com/">
                                    <img class="hovers-logos rs-grid-img" src="{{asset('public/images/partner/main-home/4.png')}}" title="" alt="">
                                    <img class="mains-logos rs-grid-img " src="{{asset('public/images/partner/main-home/4.png')}}" title="" alt="">
                                </a>
                            </div>
                            <div class="logo-img">
                                <a href="https://rstheme.com/">
                                    <img class="hovers-logos rs-grid-img" src="{{asset('public/images/partner/main-home/5.png')}}" title="" alt="">
                                    <img class="mains-logos rs-grid-img " src="{{asset('public/images/partner/main-home/5.png')}}" title="" alt="">
                                </a>
                            </div>
                            <div class="logo-img">
                                <a href="https://rstheme.com/">
                                    <img class="hovers-logos rs-grid-img" src="{{asset('public/images/partner/main-home/6.png')}}" title="" alt="">
                                    <img class="mains-logos rs-grid-img " src="{{asset('public/images/partner/main-home/6.png')}}" title="" alt="">
                                </a>
                            </div>
                            <div class="logo-img">
                                <a href="https://rstheme.com/">
                                    <img class="hovers-logos rs-grid-img" src="{{asset('public/images/partner/main-home/7.png')}}" title="" alt="">
                                    <img class="mains-logos rs-grid-img " src="{{asset('public/images/partner/main-home/7.png')}}" title="" alt="">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Premium Services Section End -->



        <!-- Services Section Start -->
        <div class="rs-services home-style2  pt-100 pb-100 md-pt-70 md-pb-70">
            <div class="container">
                <div class="sec-title2 text-center md-left mb-40">
                    <div class="sub-text">Why Choose Us</div>
                    <h2 class="title">Get our services & drive more <br><span>customers.</span></h2>
                </div>
                <div class="row y-middle">
                    <div class="col-lg-4  md-mb-50 pr-30 md-pr-l5">
                        <div class="services-item mb-45">
                            <div class="services-icon">
                                <img src="{{asset('public/images/services/style3/1.png')}}" alt="Images">
                            </div>
                            <div class="services-text">
                                <h3 class="title"><a href="services-style1.html">Expert peoples</a></h3>
                                <p class="services-txt">Quisque placerat vitae ut scelerise consulting.</p>
                            </div>
                        </div>
                        <div class="services-item mb-45">
                            <div class="services-icon">
                                <img src="{{asset('public/images/services/style3/2.png')}}" alt="Images">
                            </div>
                            <div class="services-text">
                                <h3 class="title"><a href="services-style1.html">Big experience</a></h3>
                                <p class="services-txt">Quisque placerat vitae ut scelerise consulting.</p>
                            </div>
                        </div>
                        <div class="services-item">
                            <div class="services-icon">
                                <img src="{{asset('public/images/services/style3/3.png')}}" alt="Images">
                            </div>
                            <div class="services-text">
                                <h3 class="title"><a href="services-style1.html">Financial control</a></h3>
                                <p class="services-txt">Quisque placerat vitae ut scelerise consulting.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4  md-mb-50">
                        <div class="rs-videos choose-video">
                            <div class="images-video">
                                <img src="{{asset('public/images/choose/choose-2.png')}}" alt="images">
                            </div>
                            <div class="animate-border">
                                <a class="popup-border" href="https://www.youtube.com/watch?v=FMvA5fyZ338">
                                    <i class="fa fa-play"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 pl-40 md-pl-15">
                        <div class="services-item mb-45">
                            <div class="services-icon">
                                <img src="{{asset('public/images/services/style3/4.png')}}" alt="Images">
                            </div>
                            <div class="services-text">
                                <h3 class="title"><a href="services-style1.html">Committed quality</a></h3>
                                <p class="services-txt">Quisque placerat vitae ut scelerise consulting.</p>
                            </div>
                        </div>
                        <div class="services-item mb-45">
                            <div class="services-icon">
                                <img src="{{asset('public/images/services/style3/5.png')}}" alt="Images">
                            </div>
                            <div class="services-text">
                                <h3 class="title"><a href="services-style1.html">Award winning</a></h3>
                                <p class="services-txt">Quisque placerat vitae ut scelerise consulting.</p>
                            </div>
                        </div>
                        <div class="services-item">
                            <div class="services-icon">
                                <img src="{{asset('public/images/services/style3/5.png')}}" alt="Images">
                            </div>
                            <div class="services-text">
                                <h3 class="title"><a href="services-style1.html">Insurance Policy</a></h3>
                                <p class="services-txt">Quisque placerat vitae ut scelerise consulting.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Counter Section End -->
            <div class="rs-counter style1">
                <div class="container">
                    <div class="counter-border-top">
                        <div class="row">
                            <div class="col-lg-3 col-md-6 col-sm-6 md-mb-30">
                                <div class="counter-area">
                                    <div class="counter-list mb-20">
                                        <div class="counter-icon">
                                            <img src="{{asset('public/images/counter/icons/1.png')}}" alt="Counter">
                                        </div>
                                        <div class="counter-number">
                                            <span class="rs-count">582</span>
                                        </div>
                                    </div>
                                    <div class="content-part">
                                        <h5 class="title">Projects completed for our respected clients.</h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-6 md-mb-30">
                                <div class="counter-area">
                                    <div class="counter-list mb-20">
                                        <div class="counter-icon">
                                            <img src="{{asset('public/images/counter/icons/2.png')}}" alt="Counter">
                                        </div>
                                        <div class="counter-number">
                                            <span class="rs-count">215</span>
                                            <span class="prefix">+</span>
                                        </div>
                                    </div>
                                    <div class="content-part">
                                        <h5 class="title">Experienced people serving to clients.</h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-6 xs-mb-30">
                                <div class="counter-area">
                                    <div class="counter-list mb-20">
                                        <div class="counter-icon">
                                            <img src="{{asset('public/images/counter/icons/3.png')}}" alt="Counter">
                                        </div>
                                        <div class="counter-number">
                                            <span class="rs-count">25</span>
                                            <span class="prefix">+</span>
                                        </div>
                                    </div>
                                    <div class="content-part">
                                        <h5 class="title">Years experience in business & consulting.</h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-6">
                                <div class="counter-area">
                                    <div class="counter-list mb-20">
                                        <div class="counter-icon">
                                            <img src="{{asset('public/images/counter/icons/4.png')}}" alt="Counter">
                                        </div>
                                        <div class="counter-number">
                                            <span class="rs-count">70</span>
                                            <span class="prefix">+</span>
                                        </div>
                                    </div>
                                    <div class="content-part">
                                        <h5 class="title">Business & consulting awards won over world.</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Counter Section End -->
        </div>
        <!-- Services Section End -->

        <!-- Project Section Start -->
        <div class="rs-project style7 gray-bg pt-100 pb-100 md-pt-70 md-pb-70">
            <div class="container custom">
                <div class="row y-middle">
                    <div class="col-lg-6">
                        <div class="sec-title2 mb-55 md-mb-30">
                            <div class="sub-text">Recent Work</div>
                            <h2 class="title mb-23">We blend business ideas to create something <span>awesome.</span></h2>
                        </div>
                    </div>
                    <div class="col-lg-6 pl-60 md-pl-15 mb-30">
                        <p class="desc mb-0">30+ years experience in business and finance consulting, IT solutions, and working with 5k+ clients over the world. Creation timelines for the digital consulting business theme 2021.</p>
                    </div>
                </div>
            </div>
            <div class="container-fluid pl-30 pr-30">
                <div class="rs-carousel owl-carousel"
                     data-loop="true"
                     data-items="4"
                     data-margin="30"
                     data-autoplay="false"
                     data-hoverpause="true"
                     data-autoplay-timeout="5000"
                     data-smart-speed="800"
                     data-dots="false"
                     data-nav="false"
                     data-nav-speed="false"

                     data-md-device="4"
                     data-md-device-nav="false"
                     data-md-device-dots="true"
                     data-center-mode="false"

                     data-ipad-device2="2"
                     data-ipad-device-nav2="false"
                     data-ipad-device-dots2="true"

                     data-ipad-device="2"
                     data-ipad-device-nav="false"
                     data-ipad-device-dots="true"

                     data-mobile-device="1"
                     data-mobile-device-nav="false"
                     data-mobile-device-dots="true">

                    <div class="project-item">
                        <div class="project-img">
                            <img src="{{asset('public/images/project/style2/4.jpg')}}" alt="images">
                        </div>
                        <div class="project-content">
                            <div class="project-inner">
                                <h3 class="title"><a href="project-single.html">Business planning</a></h3>
                                <span class="category"><a href="project-single.html">Investment</a></span>
                            </div>
                            <a class="p-icon" href="project-single.html"><i class="flaticon-right-arrow"></i></a>
                        </div>
                    </div>
                    <div class="project-item">
                        <div class="project-img">
                            <img src="{{asset('public/images/project/style2/2.jpg')}}" alt="images">
                        </div>
                        <div class="project-content">
                            <div class="project-inner">
                                <h3 class="title"><a href="project-single.html">Strength solutions</a></h3>
                                <span class="category"><a href="project-single.html">Investment</a></span>
                            </div>
                            <a class="p-icon" href="project-single.html"><i class="flaticon-right-arrow"></i></a>
                        </div>
                    </div>
                    <div class="project-item">
                        <div class="project-img">
                            <img src="{{asset('public/images/project/style2/3.jpg')}}" alt="images">
                        </div>
                        <div class="project-content">
                            <div class="project-inner">
                                <h3 class="title"><a href="project-single.html">Business analytics</a></h3>
                                <span class="category"><a href="project-single.html">Business Strategy</a></span>
                            </div>
                            <a class="p-icon" href="project-single.html"><i class="flaticon-right-arrow"></i></a>
                        </div>
                    </div>
                    <div class="project-item">
                        <div class="project-img">
                            <img src="{{asset('public/images/project/style2/4.jpg')}}" alt="images">
                        </div>
                        <div class="project-content">
                            <div class="project-inner">
                                <h3 class="title"><a href="project-single.html">Stock market analysis</a></h3>
                                <span class="category"><a href="project-single.html">Business Strategy</a></span>
                            </div>
                            <a class="p-icon" href="project-single.html"><i class="flaticon-right-arrow"></i></a>
                        </div>
                    </div>
                    <div class="project-item">
                        <div class="project-img">
                            <img src="{{asset('public/images/project/style2/5.jpg')}}" alt="images">
                        </div>
                        <div class="project-content">
                            <div class="project-inner">
                                <h3 class="title"><a href="project-single.html">Sales analysis</a></h3>
                                <span class="category"><a href="project-single.html">Financial</a></span>
                            </div>
                            <a class="p-icon" href="project-single.html"><i class="flaticon-right-arrow"></i></a>
                        </div>
                    </div>
                    <div class="project-item">
                        <div class="project-img">
                            <img src="{{asset('public/images/project/style2/6.jpg')}}" alt="images">
                        </div>
                        <div class="project-content">
                            <div class="project-inner">
                                <h3 class="title"><a href="project-single.html">Stock investments</a></h3>
                                <span class="category"><a href="project-single.html">Tax Consulting</a></span>
                            </div>
                            <a class="p-icon" href="project-single.html"><i class="flaticon-right-arrow"></i></a>
                        </div>
                    </div>
                    <div class="project-item">
                        <div class="project-img">
                            <img src="{{asset('public/images/project/style2/7.jpg')}}" alt="images">
                        </div>
                        <div class="project-content">
                            <div class="project-inner">
                                <h3 class="title"><a href="project-single.html">Advertising Technology</a></h3>
                                <span class="category"><a href="project-single.html">Business Strategy</a></span>
                            </div>
                            <a class="p-icon" href="project-single.html"><i class="flaticon-right-arrow"></i></a>
                        </div>
                    </div>
                    <div class="project-item">
                        <div class="project-img">
                            <img src="{{asset('public/images/project/style2/5.jpg')}}" alt="images">
                        </div>
                        <div class="project-content">
                            <div class="project-inner">
                                <h3 class="title"><a href="project-single.html">Business planning</a></h3>
                                <span class="category"><a href="project-single.html">Investment</a></span>
                            </div>
                            <a class="p-icon" href="project-single.html"><i class="flaticon-right-arrow"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Project Section End -->

        <!-- Process Section Start -->
        <div class="rs-process style1 bg2 pt-100 pb-100 md-pt-70 md-pb-70">
            <div class="container">
                <div class="row y-middle">
                    <div class="col-lg-5">
                        <div class="sec-title2 md-text-center">
                            <div class="sub-text">Working Process</div>
                            <h2 class="title mb-23 white-color">How we work for our valued  <span>customers.</span></h2>
                        </div>
                    </div>
                    <div class="col-lg-7">
                        <div class="btn-part text-right md-text-center">
                            <a class="readon consultant discover" href="portfolio.html">View Works</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container custom2">
                <div class="process-effects-layer">
                    <div class="row">
                        <div class="col-lg-3 col-md-6 md-mb-30">
                            <div class="rs-addon-number">
                                <div class="number-part">
                                    <div class="number-image">
                                        <img src="{{asset('public/images/process/style1/1.png')}}" alt="Process">
                                    </div>
                                    <div class="number-text">
                                        <div class="number-area"> <span class="number-prefix"> 1 </span></div>
                                        <div class="number-title"><h3 class="title"> Discovery</h3></div>
                                        <div class="number-txt">
                                            Quisque placerat vitae focus scelerisque. Fusce luctus odio ac nibh luctus, in porttitor.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 md-mb-30">
                            <div class="rs-addon-number">
                                <div class="number-part">
                                    <div class="number-image">
                                        <img src="{{asset('public/images/process/style1/2.png')}}" alt="Process">
                                    </div>
                                    <div class="number-text">
                                        <div class="number-area"> <span class="number-prefix"> 2 </span></div>
                                        <div class="number-title"><h3 class="title">Planning</h3></div>
                                        <div class="number-txt">
                                            Quisque placerat vitae focus scelerisque. Fusce luctus odio ac nibh luctus, in porttitor.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 sm-mb-30">
                            <div class="rs-addon-number">
                                <div class="number-part">
                                    <div class="number-image">
                                        <img src="{{asset('public/images/process/style1/3.png')}}" alt="Process">
                                    </div>
                                    <div class="number-text">
                                        <div class="number-area"> <span class="number-prefix"> 3 </span></div>
                                        <div class="number-title"><h3 class="title">Execute</h3></div>
                                        <div class="number-txt">
                                            Quisque placerat vitae focus scelerisque. Fusce luctus odio ac nibh luctus, in porttitor.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <div class="rs-addon-number">
                                <div class="number-part">
                                    <div class="number-image">
                                        <img src="{{asset('public/images/process/style1/4.png')}}" alt="Process">
                                    </div>
                                    <div class="number-text">
                                        <div class="number-area"> <span class="number-prefix"> 4 </span></div>
                                        <div class="number-title"><h3 class="title">Deliver</h3></div>
                                        <div class="number-txt">
                                            Quisque placerat vitae focus scelerisque. Fusce luctus odio ac nibh luctus, in porttitor.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Process Section End -->

        <!-- Blog Start -->
        <div class="rs-blog style2 pt-100 pb-100 md-pt-70 md-pb-70">
            <div class="container">
                <div class="row y-middle md-mb-30">
                    <div class="col-lg-5 mb-20 md-mb-10">
                        <div class="sec-title2">
                            <div class="sub-text">News Updates</div>
                            <h2 class="title mb-23">Read our latest updates & business <span>tips & tricks.</span></h2>
                        </div>
                    </div>
                    <div class="col-lg-7 mb-20">
                        <div class="btn-part text-right md-left">
                            <a class="readon consultant discover" href="blog-details.html">View Updates</a>
                        </div>
                    </div>
                </div>
                <div class="rs-carousel owl-carousel" data-loop="true" data-items="3" data-margin="30" data-autoplay="true" data-hoverpause="true" data-autoplay-timeout="5000" data-smart-speed="800" data-dots="false" data-nav="false" data-nav-speed="false" data-center-mode="false" data-mobile-device="1" data-mobile-device-nav="false" data-mobile-device-dots="true" data-ipad-device="2" data-ipad-device-nav="false" data-ipad-device-dots="true" data-ipad-device2="2" data-ipad-device-nav2="false" data-ipad-device-dots2="true" data-md-device="3" data-md-device-nav="false" data-md-device-dots="true">
                    <div class="blog-item">
                        <div class="image-wrap">
                            <a href="#"><img src="{{asset('public/images/blog/1.jpg')}}" alt="Blog"></a>
                            <ul class="post-categories">
                                <li><a href="blog-details.html">Branding</a></li>
                            </ul>
                        </div>
                        <div class="blog-content">
                            <ul class="blog-meta mb-10">
                                <li class="admin"> <i class="fa fa-user-o"></i> admin</li>
                                <li class="date"> <i class="fa fa-calendar-check-o"></i> 16 Aug 2021</li>
                            </ul>
                            <h3 class="blog-title"><a href="blog-details.html">Customer Onboarding Strategy: A Guide to Class</a></h3>
                            <p>We denounce with righteous indige nation and dislike men who are so beguiled and demo...</p>
                        </div>
                    </div>
                    <div class="blog-item">
                        <div class="image-wrap">
                            <a href="#"><img src="{{asset('public/images/blog/2.jpg')}}" alt="Blog"></a>
                            <ul class="post-categories">
                                <li><a href="blog-details.html">Branding</a></li>
                            </ul>
                        </div>
                        <div class="blog-content">
                            <ul class="blog-meta mb-10">
                                <li class="admin"> <i class="fa fa-user-o"></i> admin</li>
                                <li class="date"> <i class="fa fa-calendar-check-o"></i> 16 Aug 2021</li>
                            </ul>
                            <h3 class="blog-title"><a href="blog-details.html">How to plan a fail-proof website redesign strategy</a></h3>
                            <p>We denounce with righteous indige nation and dislike men who are so beguiled and demo...</p>
                        </div>
                    </div>
                    <div class="blog-item">
                        <div class="image-wrap">
                            <a href="#"><img src="{{asset('public/images/blog/3.jpg')}}" alt="Blog"></a>
                            <ul class="post-categories">
                                <li><a href="blog-details.html">Digital Marketing</a></li>
                            </ul>
                        </div>
                        <div class="blog-content">
                            <ul class="blog-meta mb-10">
                                <li class="admin"> <i class="fa fa-user-o"></i> admin</li>
                                <li class="date"> <i class="fa fa-calendar-check-o"></i> 16 Aug 2021</li>
                            </ul>
                            <h3 class="blog-title"><a href="blog-details.html">How investing in dependend increasing to business</a></h3>
                            <p>We denounce with righteous indige nation and dislike men who are so beguiled and demo...</p>
                        </div>
                    </div>
                    <div class="blog-item">
                        <div class="image-wrap">
                            <a href="#"><img src="{{asset('public/images/blog/4.jpg')}}" alt="Blog"></a>
                            <ul class="post-categories">
                                <li><a href="blog-details.html">Digital Marketing</a></li>
                            </ul>
                        </div>
                        <div class="blog-content">
                            <ul class="blog-meta mb-10">
                                <li class="admin"> <i class="fa fa-user-o"></i> admin</li>
                                <li class="date"> <i class="fa fa-calendar-check-o"></i> 16 Aug 2021</li>
                            </ul>
                            <h3 class="blog-title"><a href="blog-details.html">7 Productivity tips to avoid burnout when working</a></h3>
                            <p>We denounce with righteous indige nation and dislike men who are so beguiled and demo...</p>
                        </div>
                    </div>
                    <div class="blog-item">
                        <div class="image-wrap">
                            <a href="#"><img src="{{asset('public/images/blog/5.jpg')}}" alt="Blog"></a>
                            <ul class="post-categories">
                                <li><a href="blog-details.html">Graphic Design</a></li>
                            </ul>
                        </div>
                        <div class="blog-content">
                            <ul class="blog-meta mb-10">
                                <li class="admin"> <i class="fa fa-user-o"></i> admin</li>
                                <li class="date"> <i class="fa fa-calendar-check-o"></i> 16 Aug 2021</li>
                            </ul>
                            <h3 class="blog-title"><a href="blog-details.html">Email marketing tips that will increase your sales</a></h3>
                            <p>We denounce with righteous indige nation and dislike men who are so beguiled and demo...</p>
                        </div>
                    </div>
                    <div class="blog-item">
                        <div class="image-wrap">
                            <a href="#"><img src="{{asset('public/images/blog/6.jpg')}}" alt="Blog"></a>
                            <ul class="post-categories">
                                <li><a href="blog-details.html"></a></li>
                            </ul>
                        </div>
                        <div class="blog-content">
                            <ul class="blog-meta mb-10">
                                <li class="admin"> <i class="fa fa-user-o"></i> admin</li>
                                <li class="date"> <i class="fa fa-calendar-check-o"></i> 16 Aug 2021</li>
                            </ul>
                            <h3 class="blog-title"><a href="blog-details.html">How to maintain customer relations disaster strikes</a></h3>
                            <p>We denounce with righteous indige nation and dislike men who are so beguiled and demo...</p>
                        </div>
                    </div>
                    <div class="blog-item">
                        <div class="image-wrap">
                            <a href="#"><img src="{{asset('public/images/blog/1.jpg')}}" alt="Blog"></a>
                            <ul class="post-categories">
                                <li><a href="blog-details.html">E-commerce</a></li>
                            </ul>
                        </div>
                        <div class="blog-content">
                            <ul class="blog-meta mb-10">
                                <li class="admin"> <i class="fa fa-user-o"></i> admin</li>
                                <li class="date"> <i class="fa fa-calendar-check-o"></i> 16 Aug 2021</li>
                            </ul>
                            <h3 class="blog-title"><a href="blog-details.html">How to plan a fail-proof website redesign strategy</a></h3>
                            <p>We denounce with righteous indige nation and dislike men who are so beguiled and demo...</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Blog End -->

        <!-- Testimonial Section Start -->
        <div class="rs-testimonial style2 bg10 pt-100 pb-100 md-pt-70 md-pb-70">
            <div class="container">
                <div class="sec-title2 text-center md-left mb-30">
                    <div class="sub-text">Testimonials</div>
                    <h2 class="title mb-0 white-color">Whats our customers saying<br> about us</h2>
                </div>
                <div class="rs-carousel owl-carousel" data-loop="true" data-items="3" data-margin="30" data-autoplay="true" data-hoverpause="true" data-autoplay-timeout="5000" data-smart-speed="800" data-dots="false" data-nav="false" data-nav-speed="false" data-center-mode="false" data-mobile-device="1" data-mobile-device-nav="false" data-mobile-device-dots="true" data-ipad-device="2" data-ipad-device-nav="false" data-ipad-device-dots="true" data-ipad-device2="2" data-ipad-device-nav2="false" data-ipad-device-dots2="true" data-md-device="3" data-md-device-nav="false" data-md-device-dots="true">
                    <div class="testi-wrap">
                        <div class="item-content">
                            <span><img src="{{asset('public/images/testimonial/quote.png')}}" alt="Testimonial"></span>
                            <p>Customer support is excellent and documentation good – novice can easily understand. Although I had a problem with the performance, thanks to the customer support, we have solved this problem as well.</p>
                        </div>
                        <div class="testi-content">
                            <div class="image-wrap">
                                <img src="{{asset('public/images/testimonial/avatar/1.jpg')}}" alt="Testimonial">
                            </div>
                            <div class="testi-information">
                                <div class="testi-name">David Warner</div>
                                <span class="testi-title">Envato User</span>
                                <div class="ratting-img">
                                    <img src="{{asset('public/images/testimonial/ratting.png')}}" alt="Testimonial">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="testi-wrap">
                        <div class="item-content">
                            <span><img src="{{asset('public/images/testimonial/quote.png')}}" alt="Testimonial"></span>
                            <p>Customer support is excellent and documentation good – novice can easily understand. Although I had a problem with the performance, thanks to the customer support, we have solved this problem as well.</p>
                        </div>
                        <div class="testi-content">
                            <div class="image-wrap">
                                <img src="{{asset('public/images/testimonial/avatar/2.jpg')}}" alt="Testimonial">
                            </div>
                            <div class="testi-information">
                                <div class="testi-name">Emily Blunt</div>
                                <span class="testi-title">Web Developer</span>
                                <div class="ratting-img">
                                    <img src="{{asset('public/images/testimonial/ratting.png')}}" alt="Testimonial">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="testi-wrap">
                        <div class="item-content">
                            <span><img src="{{asset('public/images/testimonial/quote.png')}}" alt="Testimonial"></span>
                            <p>Customer support is excellent and documentation good – novice can easily understand. Although I had a problem with the performance, thanks to the customer support, we have solved this problem as well.</p>
                        </div>
                        <div class="testi-content">
                            <div class="image-wrap">
                                <img src="{{asset('public/images/testimonial/avatar/3.jpg')}}" alt="Testimonial">
                            </div>
                            <div class="testi-information">
                                <div class="testi-name">Ansu Fati</div>
                                <span class="testi-title">Marketing</span>
                                <div class="ratting-img">
                                    <img src="{{asset('public/images/testimonial/ratting.png')}}" alt="Testimonial">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="testi-wrap">
                        <div class="item-content">
                            <span><img src="{{asset('public/images/testimonial/quote.png')}}" alt="Testimonial"></span>
                            <p>Customer support is excellent and documentation good – novice can easily understand. Although I had a problem with the performance, thanks to the customer support, we have solved this problem as well.</p>
                        </div>
                        <div class="testi-content">
                            <div class="image-wrap">
                                <img src="{{asset('public/images/testimonial/avatar/4.jpg')}}" alt="Testimonial">
                            </div>
                            <div class="testi-information">
                                <div class="testi-name">Angelina Jolie</div>
                                <span class="testi-title">Graphic Designer</span>
                                <div class="ratting-img">
                                    <img src="assetspublic/images/testimonial/ratting.png" alt="Testimonial">
                                </div>
                            </div>
                        </div>
                    </div>
{{--                    <div class="testi-wrap">--}}
{{--                        <div class="item-content">--}}
{{--                            <span><img src="assetspublic/images/testimonial/quote.png" alt="Testimonial"></span>--}}
{{--                            <p>Customer support is excellent and documentation good – novice can easily understand. Although I had a problem with the performance, thanks to the customer support, we have solved this problem as well.</p>--}}
{{--                        </div>--}}
{{--                        <div class="testi-content">--}}
{{--                            <div class="image-wrap">--}}
{{--                                <img src="assetspublic/images/testimonial/avatar/1.jpg" alt="Testimonial">--}}
{{--                            </div>--}}
{{--                            <div class="testi-information">--}}
{{--                                <div class="testi-name">David Warner</div>--}}
{{--                                <span class="testi-title">Envato User</span>--}}
{{--                                <div class="ratting-img">--}}
{{--                                    <img src="assetspublic/images/testimonial/ratting.png" alt="Testimonial">--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="testi-wrap">--}}
{{--                        <div class="item-content">--}}
{{--                            <span><img src="assetspublic/images/testimonial/quote.png" alt="Testimonial"></span>--}}
{{--                            <p>Customer support is excellent and documentation good – novice can easily understand. Although I had a problem with the performance, thanks to the customer support, we have solved this problem as well.</p>--}}
{{--                        </div>--}}
{{--                        <div class="testi-content">--}}
{{--                            <div class="image-wrap">--}}
{{--                                <img src="assetspublic/images/testimonial/avatar/2.jpg" alt="Testimonial">--}}
{{--                            </div>--}}
{{--                            <div class="testi-information">--}}
{{--                                <div class="testi-name">Emily Blunt</div>--}}
{{--                                <span class="testi-title">Web Developer</span>--}}
{{--                                <div class="ratting-img">--}}
{{--                                    <img src="assetspublic/images/testimonial/ratting.png" alt="Testimonial">--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="testi-wrap">--}}
{{--                        <div class="item-content">--}}
{{--                            <span><img src="assetspublic/images/testimonial/quote.png" alt="Testimonial"></span>--}}
{{--                            <p>Customer support is excellent and documentation good – novice can easily understand. Although I had a problem with the performance, thanks to the customer support, we have solved this problem as well.</p>--}}
{{--                        </div>--}}
{{--                        <div class="testi-content">--}}
{{--                            <div class="image-wrap">--}}
{{--                                <img src="assetspublic/images/testimonial/avatar/3.jpg" alt="Testimonial">--}}
{{--                            </div>--}}
{{--                            <div class="testi-information">--}}
{{--                                <div class="testi-name">Ansu Fati</div>--}}
{{--                                <span class="testi-title">Marketing</span>--}}
{{--                                <div class="ratting-img">--}}
{{--                                    <img src="assetspublic/images/testimonial/ratting.png" alt="Testimonial">--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="testi-wrap">--}}
{{--                        <div class="item-content">--}}
{{--                            <span><img src="assetspublic/images/testimonial/quote.png" alt="Testimonial"></span>--}}
{{--                            <p>Customer support is excellent and documentation good – novice can easily understand. Although I had a problem with the performance, thanks to the customer support, we have solved this problem as well.</p>--}}
{{--                        </div>--}}
{{--                        <div class="testi-content">--}}
{{--                            <div class="image-wrap">--}}
{{--                                <img src="assetspublic/images/testimonial/avatar/4.jpg" alt="Testimonial">--}}
{{--                            </div>--}}
{{--                            <div class="testi-information">--}}
{{--                                <div class="testi-name">Angelina Jolie</div>--}}
{{--                                <span class="testi-title">Graphic Designer</span>--}}
{{--                                <div class="ratting-img">--}}
{{--                                    <img src="assetspublic/images/testimonial/ratting.png" alt="Testimonial">--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
                </div>
            </div>
        </div>
        <!-- Testimonial Section End -->


        <!-- Contact Section Start -->
        <div class="rs-contact contact-style2 bg11 pt-95 pb-100 md-pt-65 md-pb-70">
            <div class="container">
                <div class="sec-title2 mb-55 md-mb-35 text-center text-lg-start">
                    <div class="sub-text">Contact</div>
                    <h2 class="title mb-0">Let us help your business <br> to move <span>forward.</span></h2>
                </div>
                <div class="row y-middle">
                    <div class="col-lg-6 md-mb-50">
                        <div class="contact-img">
                            <img src="{{asset('public/images/contact/computer.jpg')}}" alt="Contact">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="contact-wrap">
                            <div id="form-messages"></div>
                            <form id="contact-form" method="post" action="https://keenitsolutions.com/products/html/bizup/mailer.php">
                                <fieldset>
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-6 mb-30">
                                            <input class="from-control" type="text" id="name" name="name" placeholder="Name" required="">
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6 mb-30">
                                            <input class="from-control" type="text" id="email" name="email" placeholder="E-Mail" required="">
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6 mb-30">
                                            <input class="from-control" type="text" id="phone" name="phone" placeholder="Phone Number" required="">
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6 mb-30">
                                            <input class="from-control" type="text" id="Website" name="subject" placeholder="Your Website" required="">
                                        </div>

                                        <div class="col-lg-12 mb-30">
                                            <textarea class="from-control" id="message" name="message" placeholder="Your message Here" required=""></textarea>
                                        </div>
                                    </div>
                                    <div class="btn-part">
                                        <div class="form-group mb-0">
                                            <input class="readon submit" type="submit" value="Submit Now">
                                        </div>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Contact Section End -->

    </div>
    <!-- Main content End -->


@endsection
